package com.pamperedchef.www.appdfailtolaunch;

import android.app.Application;
import android.app.Instrumentation;

import com.appdynamics.eumagent.runtime.AgentConfiguration;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        com.appdynamics.eumagent.runtime.Instrumentation.start(
                AgentConfiguration.builder()
                        .withAppKey("")
                        .withContext(this)
                        .withCompileTimeInstrumentationCheck(true)
                        .build()
        );
    }
}
